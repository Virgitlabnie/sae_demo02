package demo02;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyProducer {

	public static void main(String[] args) {
		new MyProducer();
	}

	MyProducer() {
		KafkaProducer<String, WorldCity> producer = new KafkaProducer<>(configureKafkaProducer());

		List<WorldCity> listWorldCities = WorldCity.reader("/home/galtier/Desktop/Enseignement/SDI/Demos/sae_demo02/worldcities.csv");	
		for (WorldCity city: listWorldCities) {
			producer.send(new ProducerRecord<String, WorldCity>("test", city.getCountry(), city));

			try { TimeUnit.SECONDS.sleep((int)(Math.random()*10)); } 
			catch (InterruptedException e) { e.printStackTrace(); }

		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
				demo02.WorldCitySerializer.class);

		return producerProperties;
	}
}