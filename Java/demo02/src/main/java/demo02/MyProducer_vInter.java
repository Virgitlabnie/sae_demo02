package demo02;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyProducer_vInter {

	public static void main(String[] args) {
		new MyProducer_vInter();
	}

	MyProducer_vInter() {
		KafkaProducer<String, String> producer = new KafkaProducer<>(configureKafkaProducer());

		List<WorldCity> listWorldCities = WorldCity.reader("/home/galtier/Desktop/Enseignement/SDI/Demos/sae_demo02/worldcities.csv");
		for (WorldCity city: listWorldCities) {
			producer.send(new ProducerRecord<String, String>("test", city.getCountry(), city.getCity()));

			try { TimeUnit.SECONDS.sleep((int)(Math.random()*10)); } 
			catch (InterruptedException e) { e.printStackTrace(); }

		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class);

		return producerProperties;
	}
}