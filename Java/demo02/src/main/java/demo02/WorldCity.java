package demo02;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author V. Galtier
 * Represents a world's city and town.
 * Source: https://simplemaps.com/data/world-cities
 */
public class WorldCity {

	/**
	 * The name of the city/town as a Unicode string (e.g. Goiânia).
	 */
	private String city;
	/**
	 * city as an ASCII string (e.g. Goiania).
	 * Left blank if ASCII representation is not possible.
	 */
	private String city_ascii;
	/**
	 * The latitude of the city/town.
	 */
	private double lat;
	/**
	 * The longitude of the city/town.
	 */
	private double lng;
	/**
	 * The name of the city/town's country.
	 */
	private String country;
	/**
	 * The alpha-2 iso code of the country.
	 */
	private String iso2;
	/**
	 * The alpha-3 iso code of the country.
	 */
	private String iso3;
	/**
	 * The name of the highest level administration region of the city town 
	 * (e.g. a US state or Canadian province). Possibly blank.
	 */
	private String admin_name;
	/**
	 * Blank string if not a capital, otherwise:
	 * 		primary - country's capital (e.g. Washington D.C.)
	 * 		admin - first-level admin capital (e.g. Little Rock, AR)
	 * 		minor - lower-level admin capital (e.g. Fayetteville, AR)
	 */
	private String capital;
	/**
	 * An estimate of the city's urban population. Only available for some (prominent) cities. 
	 * Set to -1 if not available.
	 */
	private double population;
	/**
	 * A 10-digit unique id generated by SimpleMaps.
	 */
	private int id;
	
	/**
	 * Required for Jackson deserialization
	 */
	public WorldCity() {
		super();
	}


	/**
	 * @param city The name of the city/town as a Unicode string (e.g. Goiânia).
	 * @param city_ascii city as an ASCII string (e.g. Goiania). 
	 * Left blank if ASCII representation is not possible.
	 * @param lat The latitude of the city/town.
	 * @param lng The longitude of the city/town.
	 * @param country The name of the city/town's country.
	 * @param iso2 The alpha-2 iso code of the country.
	 * @param iso3 The alpha-3 iso code of the country.
	 * @param admin_name The name of the highest level administration region of the city town 
	 * (e.g. a US state or Canadian province). Possibly blank.
	 * @param capital Blank string if not a capital, otherwise:
	 * 		primary - country's capital (e.g. Washington D.C.)
	 * 		admin - first-level admin capital (e.g. Little Rock, AR)
	 * 		minor - lower-level admin capital (e.g. Fayetteville, AR)
	 * @param population An estimate of the city's urban population or-1 if this information is not available.
	 * @param id A 10-digit unique id generated by SimpleMaps.
	 */
	public WorldCity(String city, String city_ascii, double lat, double lng, String country, String iso2, String iso3,
			String admin_name, String capital, double population, int id) {
		super();
		this.city = city;
		this.city_ascii = city_ascii;
		this.lat = lat;
		this.lng = lng;
		this.country = country;
		this.iso2 = iso2;
		this.iso3 = iso3;
		this.admin_name = admin_name;
		this.capital = capital;
		this.population = population;
		this.id = id;
	}

	/**
	 * @return The name of the city/town as a Unicode string (e.g. Goiânia).
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @return city as an ASCII string (e.g. Goiania).
	 * Left blank if ASCII representation is not possible.
	 */
	public String getCity_ascii() {
		return city_ascii;
	}
	/**
	 * @return The latitude of the city/town.
	 */
	public double getLat() {
		return lat;
	}
	/**
	 * @return The longitude of the city/town.
	 */
	public double getLng() {
		return lng;
	}
	/**
	 * @return The name of the city/town's country.
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @return The alpha-2 iso code of the country.
	 */
	public String getIso2() {
		return iso2;
	}
	/**
	 * @return The alpha-3 iso code of the country.
	 */
	public String getIso3() {
		return iso3;
	}
	/**
	 * @return admin_name The name of the highest level administration region of the city town 
	 * (e.g. a US state or Canadian province). Possibly blank.
	 */
	public String getAdmin_name() {
		return admin_name;
	}
	/**
	 * @return Blank string if not a capital, otherwise:
	 * 		primary - country's capital (e.g. Washington D.C.)
	 * 		admin - first-level admin capital (e.g. Little Rock, AR)
	 * 		minor - lower-level admin capital (e.g. Fayetteville, AR)
	 */
	public String getCapital() {
		return capital;
	}
	/**
	 * @return An estimate of the city's urban population or -1 if this information is not available.
	 */
	public double getPopulation() {
		return population;
	}
	/**
	 * @return A 10-digit unique id generated by SimpleMaps.
	 */
	public int getId() {
		return id;
	}


	@Override
	public String toString() {
		return "WorldCity [city=" + city + ", country=" + country + "]";
	}

	/**
	 * @param csvFile complete path and file name to read from.
	 * Assumption: the file complies to the CSV format of the worldcities.csv  file 
	 * from https://simplemaps.com/data/world-cities:
	 * "city","city_ascii","lat","lng","country","iso2","iso3","admin_name","capital","population","id"
	 * city_ascii, admin_name, capital and population might be blank.
	 * @return list of cities/towns from the file
	 */
	public static List<WorldCity> reader(String csvFile) {

		List<WorldCity> listWorldCities = new ArrayList<WorldCity>();

		try {
			// all variables are declared here, to avoid creations in the loop
			String line = null; // a line of the CSV file
			int indexOfQuote; // each field is framed by " characters

			String city;
			String city_ascii;
			double lat;
			double lng;
			String country;
			String iso2;
			String iso3;
			String admin_name;
			String capital;
			double population;
			int id;

			BufferedReader bufferedReader = new BufferedReader(new FileReader(csvFile));
			bufferedReader.readLine(); // first line contains column header so we read it before the loop

			while (((line = bufferedReader.readLine()) != null) && (!line.equals(""))) {
				
				line = line.substring(1);
				indexOfQuote = line.indexOf("\"");
				city = line.substring(0, indexOfQuote);
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				city_ascii = line.substring(0, indexOfQuote);
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				lat = Double.parseDouble(line.substring(0, indexOfQuote));
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				lng = Double.parseDouble(line.substring(0, indexOfQuote));
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				country = line.substring(0, indexOfQuote);
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				iso2 = line.substring(0, indexOfQuote);
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				iso3 = line.substring(0, indexOfQuote);
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				admin_name = line.substring(0, indexOfQuote);
								
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				capital = line.substring(0, indexOfQuote);
				if (!capital.contentEquals("") && !capital.contentEquals("primary") && !capital.contentEquals("admin") && !capital.contentEquals("minor")) {
					capital = "";
				}
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				try {
				population = Double.parseDouble(line.substring(0, indexOfQuote));
				} catch (NumberFormatException nfe) {
					population = -1;
				}
				
				line = line.substring(indexOfQuote+3);
				indexOfQuote = line.indexOf("\"");
				id = Integer.parseInt(line.substring(0, indexOfQuote));
				
				listWorldCities.add(new WorldCity(city, city_ascii, lat, lng, country, iso2, iso3, admin_name, capital, population, id));
			}
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listWorldCities;
	}
}