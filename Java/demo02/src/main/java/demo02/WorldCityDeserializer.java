package demo02;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WorldCityDeserializer implements Deserializer<WorldCity> {

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public WorldCity deserialize(String topic, byte[] bytes) {
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		if (bytes == null)
			return null;

		try {
			return objectMapper.readValue(bytes, WorldCity.class);
		} catch (Exception e) {
			throw new SerializationException("Error deserializing JSON message", e);
		}       
	}
}