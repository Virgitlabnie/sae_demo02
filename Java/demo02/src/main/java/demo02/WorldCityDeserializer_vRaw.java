package demo02;

import org.apache.kafka.common.serialization.Deserializer;

public class WorldCityDeserializer_vRaw implements Deserializer<WorldCity> {

	@Override
	public WorldCity deserialize(String topic, byte[] data) {
		
		String line = new String(data);

		line = line.substring(1);
		int indexOfQuote = line.indexOf("\"");
		String city = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String city_ascii = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		double lat = Double.parseDouble(line.substring(0, indexOfQuote));

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		double lng = Double.parseDouble(line.substring(0, indexOfQuote));

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String country = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String iso2 = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String iso3 = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String admin_name = line.substring(0, indexOfQuote);

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		String capital = line.substring(0, indexOfQuote);
		if (!capital.contentEquals("") && !capital.contentEquals("primary") && !capital.contentEquals("admin") && !capital.contentEquals("minor")) {
			capital = "";
		}

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		double population = -1;
		try {
			population = Double.parseDouble(line.substring(0, indexOfQuote));
		} catch (NumberFormatException nfe) {
			population = -1;
		}

		line = line.substring(indexOfQuote+3);
		indexOfQuote = line.indexOf("\"");
		int id = Integer.parseInt(line.substring(0, indexOfQuote));

		return new WorldCity(city, city_ascii, lat, lng, country, iso2, iso3, admin_name, capital, population, id);
	}

}