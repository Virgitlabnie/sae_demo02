package demo02;

import java.nio.ByteBuffer;

import org.apache.kafka.common.serialization.Serializer;

public class  WorldCitySerializer_vRaw implements Serializer<WorldCity> {

	/**
	 * Serialize using the same format as the initial CSV file:
	 * "city","city_ascii","lat","lng","country","iso2","iso3","admin_name","capital","population","id"
	 * data types: String city, String city_ascii, double lat, 
	 * double lng, String country, String iso2, String iso3, 
	 * String admin_name, String capital, double population, int id
	 */
	@Override
	public byte[] serialize(String topic, WorldCity data) {

		if (data == null) {
			return null;
		
		} else {
			String wrapperChar = "\"";
			String separator = ",";
			String serializedWorldCityString = wrapperChar + data.getCity() + wrapperChar + separator +
					wrapperChar + data.getCity_ascii() + wrapperChar + separator +
					wrapperChar + data.getLat() + wrapperChar + separator +
					wrapperChar + data.getLng() + wrapperChar + separator +
					wrapperChar + data.getCountry() + wrapperChar + separator +
					wrapperChar + data.getIso2() + wrapperChar + separator +
					wrapperChar + data.getIso3() + wrapperChar + separator +
					wrapperChar + data.getAdmin_name() + wrapperChar + separator +
					wrapperChar + data.getCapital() + wrapperChar + separator +
					wrapperChar + data.getPopulation() + wrapperChar + separator +
					wrapperChar + data.getId() + wrapperChar + separator;
			byte[] serializedWorldCityStringBytes = serializedWorldCityString.getBytes();

			ByteBuffer buffer = ByteBuffer.allocate(serializedWorldCityStringBytes.length);
			buffer.put(serializedWorldCityStringBytes);

			return buffer.array();
		}
	}
}