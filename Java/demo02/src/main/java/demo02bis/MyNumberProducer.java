package demo02bis;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyNumberProducer {
	public static void main(String[] args) {
		new MyNumberProducer();
	}

	MyNumberProducer() {
		KafkaProducer<String, Number> producer = new KafkaProducer<>(configureKafkaProducer());

		int nb = 0;
		while (true) {
			producer.send(new ProducerRecord<String, Number>("test", null, new Number(nb)));
			nb++;

			try {
				TimeUnit.SECONDS.sleep((int) (Math.random() * 3));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, demo02bis.NumberSerializer.class);

		return producerProperties;
	}
}
