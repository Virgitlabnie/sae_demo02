package demo02bis;

public class Number {
	private int number;

	public Number(int number) {
		super();
		this.number = number;
	}

	public Number() {
		super();
	}

	@Override
	public String toString() {
		return "Number [number=" + number + "]";
	}

}
