package demo02bis;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NumberDeserializer implements Deserializer<Number> {

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public Number deserialize(String topic, byte[] bytes) {
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		if (bytes == null)
			return null;

		try {
			/*
			 * String jsonString = new String(bytes); System.out.println("JSON string: " +
			 * jsonString); Number number = objectMapper.readValue(jsonString,
			 * Number.class); System.out.println(number);
			 */
			return objectMapper.readValue(bytes, Number.class);
		} catch (Exception e) {
			throw new SerializationException("Error deserializing JSON message", e);
		}
	}
}
