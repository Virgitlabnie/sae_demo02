package demo02bis;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NumberSerializer implements Serializer<Number> {

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public byte[] serialize(String topic, Number data) {
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		if (data == null)
			return null;

		try {
			/*
			 * String jsonString = objectMapper.writeValueAsString(data);
			 * System.out.println("JSON string: " + jsonString);
			 */
			return objectMapper.writeValueAsBytes(data);
		} catch (Exception e) {
			throw new SerializationException("Error serializing JSON message", e);
		}
	}

}
